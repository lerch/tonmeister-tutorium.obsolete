\select@language {german}
\contentsline {chapter}{\numberline {1}Schwingungen und Wellen}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Schwingungen}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Frequenz und Periodendauer}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Amplitude}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Phasenverschiebung}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}T\"one, Kl\"ange und Klangfarben}{6}{section.1.5}
\contentsline {section}{\numberline {1.6}Wellen}{8}{section.1.6}
\contentsline {section}{\numberline {1.7}Ausbreitungsgeschwindigkeit}{8}{section.1.7}
\contentsline {section}{\numberline {1.8}Wellenl\"ange}{9}{section.1.8}
\contentsline {section}{\numberline {1.9}Ausbreitung von Wellen}{10}{section.1.9}
\contentsline {section}{\numberline {1.10}Zusammenfassung}{10}{section.1.10}
\contentsline {section}{\numberline {1.11}Aufgaben}{11}{section.1.11}
\contentsline {chapter}{\numberline {2}Schall und Schallfeld}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Schallschnelle und Schalldruck}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Entfernungsabh\"angigkeit des Schalldrucks}{15}{section.2.2}
\contentsline {section}{\numberline {2.3}Nahfeld und Fernfeld/ Entfernungs\discretionary {-}{}{}ab\discretionary {-}{}{}h\"an\discretionary {-}{}{}gig\discretionary {-}{}{}keit der Schallschnelle}{15}{section.2.3}
\contentsline {section}{\numberline {2.4}Schalleistung}{17}{section.2.4}
\contentsline {section}{\numberline {2.5}Schallintensit\"at}{17}{section.2.5}
\contentsline {section}{\numberline {2.6}Gest\"orte Schallausbreitung}{17}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Reflexion}{18}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Beugung}{19}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}Zusammenfassung}{19}{section.2.7}
\contentsline {section}{\numberline {2.8}Aufgaben}{20}{section.2.8}
\contentsline {chapter}{\numberline {3}Pegel}{21}{chapter.3}
\contentsline {section}{\numberline {3.1}Das Weber-Fechnersche Gesetz}{21}{section.3.1}
\contentsline {section}{\numberline {3.2}Schalldruckpegel \& Co.}{22}{section.3.2}
\contentsline {section}{\numberline {3.3}Pegel bei der Addition von Signalen}{24}{section.3.3}
\contentsline {section}{\numberline {3.4}Zusammenfassung}{28}{section.3.4}
\contentsline {section}{\numberline {3.5}Aufgaben}{28}{section.3.5}
\contentsline {chapter}{\numberline {4}Raumakustik}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Nachhallzeit}{32}{section.4.1}
\contentsline {section}{\numberline {4.2}Absorption}{33}{section.4.2}
\contentsline {section}{\numberline {4.3}Direktschallfeld und Diffusschallfeld}{33}{section.4.3}
\contentsline {section}{\numberline {4.4}Hallradius}{33}{section.4.4}
\contentsline {section}{\numberline {4.5}Frequenzabh\"angigkeit des Nachhalls}{36}{section.4.5}
\contentsline {section}{\numberline {4.6}Flatterecho}{36}{section.4.6}
\contentsline {section}{\numberline {4.7}Stehende Wellen}{37}{section.4.7}
\contentsline {section}{\numberline {4.8}Zusammenfassung}{38}{section.4.8}
\contentsline {section}{\numberline {4.9}Aufgaben}{39}{section.4.9}
\contentsline {chapter}{\numberline {5}Psychoakustik}{41}{chapter.5}
\contentsline {section}{\numberline {5.1}Physiologie des Ohres}{41}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Das Au\IeC {\ss }enohr}{41}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Das Mittelohr}{42}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Das Innenohr}{43}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Schalldruckpegel, Lautst\"arke und Lautheit}{45}{section.5.2}
\contentsline {section}{\numberline {5.3}Tonh\"ohenempfindung}{46}{section.5.3}
\contentsline {section}{\numberline {5.4}Verdeckungseffekte}{46}{section.5.4}
\contentsline {section}{\numberline {5.5}Richtungsh\"oren bei einer Schallquelle}{48}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Das Gesetz der ersten Wellenfront}{49}{subsection.5.5.1}
\contentsline {section}{\numberline {5.6}Richtungsh\"oren im Stereodreieck}{52}{section.5.6}
\contentsline {section}{\numberline {5.7}Zusammenfassung}{52}{section.5.7}
\contentsline {section}{\numberline {5.8}Aufgaben}{54}{section.5.8}
\contentsline {chapter}{\numberline {6}Signalverarbeitung}{55}{chapter.6}
\contentsline {section}{\numberline {6.1}Signalbeschreibungen}{55}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Zeitverlauf}{55}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Spektrum}{55}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Amplitudendichteverteilung}{57}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Systembeschreibungen}{60}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Lineare Systeme}{60}{subsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.1.1}Faltung}{61}{subsubsection.6.2.1.1}
\contentsline {subsection}{\numberline {6.2.2}Nichtlineare Systeme}{61}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Grundlegende Signalformen bei der Signalsynthese}{61}{section.6.3}
\contentsline {section}{\numberline {6.4}Ger\"ate der Signalverarbeitung}{63}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Fader}{63}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Filter}{63}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}Delay und Delay-basierte Effekte}{66}{subsection.6.4.3}
\contentsline {subsection}{\numberline {6.4.4}Hallger\"at}{67}{subsection.6.4.4}
\contentsline {subsection}{\numberline {6.4.5}Dynamikbearbeitung}{68}{subsection.6.4.5}
\contentsline {subsection}{\numberline {6.4.6}Verzerrer und Enhancer}{69}{subsection.6.4.6}
\contentsline {subsection}{\numberline {6.4.7}andere Effekte}{69}{subsection.6.4.7}
\contentsline {section}{\numberline {6.5}Qualit\"atsmerkmale}{69}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}\"Ubertragungsfrequenzgang}{69}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}Dynamikumfang}{70}{subsection.6.5.2}
\contentsline {subsubsection}{\numberline {6.5.2.1}Bewertungsfilter}{70}{subsubsection.6.5.2.1}
\contentsline {subsection}{\numberline {6.5.3}Klirrfaktor}{70}{subsection.6.5.3}
\contentsline {subsection}{\numberline {6.5.4}Total Harmonic Distortion and Noise}{71}{subsection.6.5.4}
\contentsline {subsection}{\numberline {6.5.5}Modulationsverzerrungen}{71}{subsection.6.5.5}
\contentsline {subsection}{\numberline {6.5.6}\"Ubersprechd\"ampfung}{72}{subsection.6.5.6}
\contentsline {subsection}{\numberline {6.5.7}H\"ortests}{72}{subsection.6.5.7}
\contentsline {section}{\numberline {6.6}Zusammenfassung}{72}{section.6.6}
\contentsline {section}{\numberline {6.7}Aufgaben}{73}{section.6.7}
\contentsline {chapter}{\numberline {7}Elektroakustische Wandler}{75}{chapter.7}
\contentsline {section}{\numberline {7.1}Mikrophone}{75}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Charakteristische Kenngr\"o\IeC {\ss }en}{75}{subsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.1.1}Pegel-Kenngr\"o\IeC {\ss }en}{75}{subsubsection.7.1.1.1}
\contentsline {subsubsection}{\numberline {7.1.1.2}\"Ubertragungsfrequenzgang}{76}{subsubsection.7.1.1.2}
\contentsline {subsubsection}{\numberline {7.1.1.3}Richtcharakteristik}{77}{subsubsection.7.1.1.3}
\contentsline {subsubsection}{\numberline {7.1.1.4}Impulsverhalten}{79}{subsubsection.7.1.1.4}
\contentsline {subsection}{\numberline {7.1.2}Mikrophontypen}{79}{subsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.2.1}Kondensatormikrophon}{80}{subsubsection.7.1.2.1}
\contentsline {paragraph}{Mechanischer Aufbau\\}{80}{section*.3}
\contentsline {paragraph}{Elektrischer Aufbau\\}{82}{section*.4}
\contentsline {subsubsection}{\numberline {7.1.2.2}Dynamisches Mikrophon}{83}{subsubsection.7.1.2.2}
\contentsline {paragraph}{Mechanischer Aufbau\\}{83}{section*.5}
\contentsline {paragraph}{Elektrischer Aufbau\\}{84}{section*.6}
\contentsline {subsubsection}{\numberline {7.1.2.3}Andere Mikrophontypen}{84}{subsubsection.7.1.2.3}
\contentsline {subsection}{\numberline {7.1.3}Bauform}{84}{subsection.7.1.3}
\contentsline {subsubsection}{\numberline {7.1.3.1}Druckempf\"anger}{84}{subsubsection.7.1.3.1}
\contentsline {subsubsection}{\numberline {7.1.3.2}Druckgradientenempf\"anger}{84}{subsubsection.7.1.3.2}
\contentsline {subsubsection}{\numberline {7.1.3.3}Mischformen}{84}{subsubsection.7.1.3.3}
\contentsline {subsubsection}{\numberline {7.1.3.4}Andere Bauformen}{85}{subsubsection.7.1.3.4}
\contentsline {section}{\numberline {7.2}Lautsprecher}{85}{section.7.2}
\contentsline {section}{\numberline {7.3}Zusammenfassung}{85}{section.7.3}
\contentsline {section}{\numberline {7.4}Aufgaben}{85}{section.7.4}
\contentsline {chapter}{\numberline {8}Digitaltechnik}{87}{chapter.8}
\contentsline {section}{\numberline {8.1}Bin\"are Zahlendarstellung}{87}{section.8.1}
\contentsline {section}{\numberline {8.2}Digitalisierung eines Analogsignals}{89}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Abtastung}{89}{subsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.1.1}periodisches Spektrum}{89}{subsubsection.8.2.1.1}
\contentsline {subsubsection}{\numberline {8.2.1.2}Abtasttheorem und Aliasing}{91}{subsubsection.8.2.1.2}
\contentsline {subsubsection}{\numberline {8.2.1.3}Rekonstruktion des Signals}{92}{subsubsection.8.2.1.3}
\contentsline {subsection}{\numberline {8.2.2}Quantisierung}{92}{subsection.8.2.2}
\contentsline {subsubsection}{\numberline {8.2.2.1}Quantisierungsfehler}{93}{subsubsection.8.2.2.1}
\contentsline {subsubsection}{\numberline {8.2.2.2}Oversampling}{95}{subsubsection.8.2.2.2}
\contentsline {subsubsection}{\numberline {8.2.2.3}Dither}{96}{subsubsection.8.2.2.3}
\contentsline {subsubsection}{\numberline {8.2.2.4}Noise-Shaping}{98}{subsubsection.8.2.2.4}
\contentsline {subsection}{\numberline {8.2.3}Wandler}{100}{subsection.8.2.3}
\contentsline {subsubsection}{\numberline {8.2.3.1}Analog-Digital-Wandler}{100}{subsubsection.8.2.3.1}
\contentsline {subsubsection}{\numberline {8.2.3.2}Digital-Analog-Wandler}{100}{subsubsection.8.2.3.2}
\contentsline {section}{\numberline {8.3}Digitale Filter}{100}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Filterstrukturen}{105}{subsection.8.3.1}
\contentsline {section}{\numberline {8.4}Speichermedien}{105}{section.8.4}
\contentsline {subsection}{\numberline {8.4.1}Magnetb\"ander}{105}{subsection.8.4.1}
\contentsline {subsection}{\numberline {8.4.2}Optische Medien}{105}{subsection.8.4.2}
\contentsline {section}{\numberline {8.5}\"Ubertragungstechnik/Fehlerbehandlung}{105}{section.8.5}
\contentsline {subsection}{\numberline {8.5.1}Fehlerquellen bei der \"Ubertragung}{105}{subsection.8.5.1}
\contentsline {subsection}{\numberline {8.5.2}Jitter}{105}{subsection.8.5.2}
\contentsline {subsection}{\numberline {8.5.3}Kanalcodes}{105}{subsection.8.5.3}
\contentsline {subsubsection}{\numberline {8.5.3.1}Fehlerrobustheit}{105}{subsubsection.8.5.3.1}
\contentsline {subsubsection}{\numberline {8.5.3.2}Fehlererkennung}{105}{subsubsection.8.5.3.2}
\contentsline {subsubsection}{\numberline {8.5.3.3}Fehlerbehandlung}{105}{subsubsection.8.5.3.3}
\contentsline {section}{\numberline {8.6}Codierungsverfahren}{105}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}\"Uberblick \"uber Codierungsverfahren}{106}{subsection.8.6.1}
\contentsline {subsection}{\numberline {8.6.2}Wo k\"onnen Bits gespart werden}{106}{subsection.8.6.2}
\contentsline {subsection}{\numberline {8.6.3}Redundanzcodierung}{106}{subsection.8.6.3}
\contentsline {subsection}{\numberline {8.6.4}Irrelevanzcodierung}{107}{subsection.8.6.4}
\contentsline {subsubsection}{\numberline {8.6.4.1}Grunds\"atzlicher Aufbau verlustbehafteter Verfahren}{107}{subsubsection.8.6.4.1}
\contentsline {subsubsection}{\numberline {8.6.4.2}Typische Artefakte}{108}{subsubsection.8.6.4.2}
\contentsline {subsubsection}{\numberline {8.6.4.3}Qualit\"atsbeurteilung}{109}{subsubsection.8.6.4.3}
\contentsline {subsubsection}{\numberline {8.6.4.4}Encodieroptionen}{110}{subsubsection.8.6.4.4}
\contentsline {subsubsection}{\numberline {8.6.4.5}Auswahlkriterien von Codierungsverfahren}{110}{subsubsection.8.6.4.5}
\contentsline {section}{\numberline {8.7}Zusammenfassung}{112}{section.8.7}
\contentsline {section}{\numberline {8.8}Aufgaben}{112}{section.8.8}
\contentsline {part}{Anhang}{114}{Item.61}
\contentsline {chapter}{\numberline {A}L\"osungen der Aufgaben}{117}{appendix.A}
\contentsline {section}{\numberline {A.1}Schwingungen und Wellen}{117}{section.A.1}
\contentsline {section}{\numberline {A.2}Schall und Schallfeld}{119}{section.A.2}
\contentsline {section}{\numberline {A.3}Pegel}{120}{section.A.3}
\contentsline {section}{\numberline {A.4}Raumakustik}{122}{section.A.4}
\contentsline {section}{\numberline {A.5}Psychoakustik}{123}{section.A.5}
\contentsline {section}{\numberline {A.6}Elektrotechnik}{124}{section.A.6}
\contentsline {section}{\numberline {A.7}Signalverarbeitung}{124}{section.A.7}
\contentsline {section}{\numberline {A.8}Mikrophone}{125}{section.A.8}
\contentsline {section}{\numberline {A.9}Digitaltechnik}{125}{section.A.9}
\contentsline {chapter}{Verzeichnis der Abk\"urzungen und Formelzeichen}{126}{Item.104}
\contentsline {chapter}{Abbildungsverzeichnis}{129}{table.A.3}
\contentsline {chapter}{Tabellenverzeichnis}{132}{appendix*.9}
\contentsline {chapter}{Literaturverzeichnis}{133}{appendix*.10}
\contentsline {chapter}{Index}{137}{appendix*.11}
